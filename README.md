# Python BDD Interview Assignment
Python BDD application that will be used by our candidates.

## Pre-requisite
  - Python 3
  - Pip
  - Python Behave or BDD (pytest-bdd)

## Dependencies
Packages are predefined in the given Pipfile. Candidates are allowed to add/update/remove any dependencies.
  
## Requirement
  - Candidates should implement required scenario(s)
  - Implementation should demonstrate understanding of BDD and Python best practices

### Required scenario(s)
  - [ ] Search property in [SPEEDHOME main page][2] and will see search result page
  - [ ] Clicking login in [SPEEDHOME main page][2] will be redirected to https://auth.speedhome.com/
  - [ ] Language changing is expected in [Landlord FAQ page][3]

## Nice to Have
  - Selenium Grid
  - Docker support
  - Docker-compose support

## Candidates can submit their work via
  - [Pull Request][1]
  - Cloud storage of their choice
  - Email to recruitment@speedhome.com


[1]: https://bitbucket.org/speedrent/python-bdd-interview/pull-requests/
[2]: https://speedhome.com
[3]: https://speedhome.com/learn/landlord-faq
